# Pig-latin Translator

This is a pig-latin translator for a Citrix interview.

- I have decided to use Angular mainly because I know the environment and it has allowed me to focus on what was important rather than on the initial project setup.
- I could have added some tests for the `AppComponent`, but I believe the unit test examples for the two services should be enough for the purposes of this exercise.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
