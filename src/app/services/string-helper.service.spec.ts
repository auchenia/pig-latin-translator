import { TestBed } from '@angular/core/testing';

import { StringHelperService } from './string-helper.service';

describe('StringHelperService', () => {
  let service: StringHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StringHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('endsWithWay', () => {
    it('should handle input ending with "way"', () => {
      expect(service.endsWithWay('stairway'))
        .toEqual(true);
    });

    it('should handle input not ending with "way"', () => {
      expect(service.endsWithWay('hello'))
        .toEqual(false);
    });
  });

  describe('getCapitalLetterIndexes', () => {
    it('should handle capitalised input', () => {
      expect(service.getCapitalLetterIndexes('Beach'))
        .toEqual([true, false, false, false, false]);
    });

    it('should handle Irish names', () => {
      expect(service.getCapitalLetterIndexes('McCloud'))
        .toEqual([true, false, true, false, false, false, false]);
    });

    it('should not consider non-A-Z symbols as capital letters', () => {
      expect(service.getCapitalLetterIndexes('small_test'))
        .toEqual([false, false, false, false, false, false, false, false, false, false]);
    });
  });

  describe('getPunctuationIndexes', () => {
    it('should handle input with punctuation', () => {
      expect(service.getPunctuationIndexes(`can't`))
        .toEqual([3]);
    });

    it('should handle input without punctuation', () => {
      expect(service.getPunctuationIndexes('end'))
        .toEqual([]);
    });
  });

  describe('startsWithConsonant', () => {
    it('should handle input starting with a consonant', () => {
      expect(service.startsWithConsonant('hello'))
        .toEqual(true);
    });

    it('should ignore input starting with a number', () => {
      expect(service.startsWithConsonant('007'))
        .toEqual(false);
    });

    it('should handle input starting with a vowel', () => {
      expect(service.startsWithConsonant('apple'))
        .toEqual(false);
    });
  });

  describe('startsWithVowel', () => {
    it('should handle input starting with a vowel', () => {
      expect(service.startsWithVowel('apple'))
        .toEqual(true);
    });

    it('should ignore input starting with a consonant', () => {
      expect(service.startsWithVowel('hello'))
        .toEqual(false);
    });

    it('should ignore input starting with a number', () => {
      expect(service.startsWithVowel('007'))
        .toEqual(false);
    });
  });
});
