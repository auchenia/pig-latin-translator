import { TestBed } from '@angular/core/testing';

import { PigLatinTranslationService } from './pig-latin-translation.service';
import { StringHelperService } from './string-helper.service';

describe('PigLatinTranslationService', () => {
  let service: PigLatinTranslationService;
  let stringHelperServiceSpy: jasmine.SpyObj<StringHelperService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: StringHelperService,
          useValue: jasmine.createSpyObj('StringHelperService', [
            'endsWithWay',
            'getCapitalLetterIndexes',
            'getPunctuationIndexes'
          ])
        }
      ]
    });

    service = TestBed.inject(PigLatinTranslationService);
    stringHelperServiceSpy = TestBed.inject(StringHelperService) as jasmine.SpyObj<StringHelperService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('appendAy', () => {
    it('should move first letter and append AY', () => {
      stringHelperServiceSpy.getCapitalLetterIndexes.and.returnValue([true, false, false, false, false]);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([]);

      expect(service.appendAy('Hello'))
        .toEqual('Ellohay');
    });

    it('should handle words like "SaaS', () => {
      stringHelperServiceSpy.getCapitalLetterIndexes.and.returnValue([true, false, false, true]);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([]);

      expect(service.appendAy('SaaS'))
        .toEqual('AasSay');
    });

    it('should handle words with punctuation', () => {
      stringHelperServiceSpy.getCapitalLetterIndexes.and.returnValue([false, false, false, false]);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([3]);

      expect(service.appendAy(`can't`))
        .toEqual(`antca'y`);
    });

    it('should handle words with multi-punctuation', () => {
      stringHelperServiceSpy.getCapitalLetterIndexes.and.returnValue([false, false, false, false, false]);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([2, 4]);

      expect(service.appendAy(`ca'n't`))
        .toEqual(`antc'a'y`);
    });
  });

  describe('appendWay', () => {
    it('should append "way" when the word does not end with "way"', () => {
      stringHelperServiceSpy.endsWithWay.and.returnValue(false);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([]);

      expect(service.appendWay('apple'))
        .toEqual('appleway');
    });

    it('should handle words like "AFK"', () => {
      stringHelperServiceSpy.endsWithWay.and.returnValue(false);
      stringHelperServiceSpy.getCapitalLetterIndexes.and.returnValue([true, true, true]);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([]);

      expect(service.appendWay('AFK'))
        .toEqual('AFKway');
    });

    it('should handle words with punctuation', () => {
      stringHelperServiceSpy.endsWithWay.and.returnValue(false);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([3]);

      expect(service.appendWay(`ain't`))
        .toEqual(`aintwa'y`);
    });

    it('should not append "way" when the word does end with "way"', () => {
      stringHelperServiceSpy.endsWithWay.and.returnValue(true);
      stringHelperServiceSpy.getPunctuationIndexes.and.returnValue([]);

      expect(service.appendWay('stairway'))
        .toEqual('stairway');
    });
  });
});
