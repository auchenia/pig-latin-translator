import { Injectable } from '@angular/core';

import { StringHelperService } from './string-helper.service';

@Injectable({
  providedIn: 'root'
})
export class PigLatinTranslationService {
  constructor(
    private stringHelperService: StringHelperService
  ) {
    //
  }

  public appendAy(word: string): string {
    const capitalLetters = this.stringHelperService.getCapitalLetterIndexes(word);
    const punctuation = this.stringHelperService.getPunctuationIndexes(word);
    const lowerCasedWord = word.toLowerCase();
    const processedWord = `${lowerCasedWord}${lowerCasedWord[0]}ay`
      .replace(/'/ig, '')
      .slice(1)
      .split('')
      .map((letter, index) => capitalLetters[index] ? letter.toUpperCase() : letter)
      .join('');

    return this.reInsertPunctuation(processedWord, punctuation, 2);
  }

  public appendWay(word: string): string {
    const endsWithWay = this.stringHelperService.endsWithWay(word);
    if (endsWithWay) {
      return word;
    }

    const punctuation = this.stringHelperService.getPunctuationIndexes(word);
    const processedWord = word.replace(/'/ig, '');
    let appendedWord = `${processedWord}way`;

    return this.reInsertPunctuation(appendedWord, punctuation, 3);
  }

  public reInsertPunctuation(word: string, punctuation: number[], offset: number): string {
    let result = word;

    punctuation
      .map(index => index + offset)
      .forEach(index => {
        result = `${result.slice(0, index)}'${result.slice(index)}`;
      });

    return result;
  }
}
