import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StringHelperService {
  constructor() {
    //
  }

  public endsWithWay(input: string): boolean {
    return /way$/i.test(input);
  }

  public getCapitalLetterIndexes(word: string): boolean[] {
    return word.split('')
      .map(letter => /[a-z]/i.test(letter) && letter === letter.toUpperCase());
  }

  public getPunctuationIndexes(word: string): number[] {
    return word.split('')
      .map((letter, index) => /'/i.test(letter) ? index : null)
      .filter(index => index !== null);
  }

  public startsWithConsonant(input: string): boolean {
    return /^[bcdfghjklmnpqrstvwxz]/i.test(input);
  }

  public startsWithVowel(input: string): boolean {
    return /^[aeiouy]/i.test(input);
  }
}
