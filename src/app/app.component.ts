import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

import { PigLatinTranslationService } from './services/pig-latin-translation.service';
import { StringHelperService } from './services/string-helper.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public readonly form: FormGroup;
  public readonly textOutput$: Observable<string>;

  constructor(
    formBuilder: FormBuilder,
    pigLatinTranslationService: PigLatinTranslationService,
    stringHelperService: StringHelperService
  ) {
    this.form = formBuilder.group({
      textInput: '',
      textOutput: ''
    });

    this.textOutput$ = this.form.controls.textInput.valueChanges
      .pipe(
        debounceTime(500),
        map(textInput => textInput.replace(/[\w\d_']+/ig, word => {
          if (stringHelperService.startsWithConsonant(word)) {
            return pigLatinTranslationService.appendAy(word);
          }

          if (stringHelperService.startsWithVowel(word)) {
            return pigLatinTranslationService.appendWay(word);
          }

          return word;
        }))
      );
  }
}
